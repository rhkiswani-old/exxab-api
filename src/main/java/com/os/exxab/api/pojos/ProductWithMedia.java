package com.os.exxab.api.pojos;

import java.util.List;

import com.google.code.magja.model.product.ProductType;

public class ProductWithMedia {

	private List<String> images;
	private int id;
	private String sku = "N/A";
	private String name = "N/A";
	private String description = "N/A";
	private String color;
	private String status;
	private String specialPrice = "N/A";
	private String stockAvailability = "N/A";
	private String size = "N/A";
	private Double weight = 0d;
	private Double price = 0d;
	private Double qty = 0d;
	private String shortDescription = "N/A";
	private String condition;
	private String cost;

	public List<String> getImages() {
		return images;
	}

	public void setImages(List<String> images) {
		this.images = images;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescrition(String description) {
		this.description = description;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getPrice() {
		return price;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public Double getQty() {
		return qty;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSpecialPrice() {
		return specialPrice;
	}

	public void setSpecialPrice(String specialPrice) {
		this.specialPrice = specialPrice;
	}

	public String getStockAvailability() {
		return stockAvailability;
	}

	public void setStockAvailability(String stockAvailability) {
		this.stockAvailability = stockAvailability;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public void setShortDesc(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getCondition() {
		return condition;
	}

	public String getCost() {
		return cost;
	}

}

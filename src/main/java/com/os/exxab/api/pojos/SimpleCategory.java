package com.os.exxab.api.pojos;

public class SimpleCategory {

	private Integer id;
	private Integer parentId;
	private String name;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "SimpleCategory [id=" + id + ", parentId=" + parentId + ", name=" + name + "]";
	}

	
}

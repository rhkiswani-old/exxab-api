package com.os.exxab.api.pojos;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author mohamed
 *
 */
public class CategoryWithChildern {

	private Integer id;
	private String name;
	private List<CategoryWithChildern> childern = new ArrayList<CategoryWithChildern>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<CategoryWithChildern> getChildern() {
		return childern;
	}

	public void setChildern(List<CategoryWithChildern> childern) {
		this.childern = childern;
	}
	
	public void addChild(CategoryWithChildern child) {
		this.childern.add(child);
	}

}

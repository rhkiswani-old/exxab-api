package com.os.exxab.api.controllers;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.os.exxab.api.service.UsersService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 */
@Path("/users")
@Api("/users")
@Produces(MediaType.APPLICATION_JSON)
public class UsersController extends GenericController{

	private UsersService service = new UsersService();

	@GET
	@Path("getUserPassword")
	@ApiOperation("find Customer By Email")
	public Response findCustomerByEmail(
			@QueryParam("username") String username,
			@QueryParam("password") String password,
			@QueryParam("authKey") String authKey) {
		try {
			if (authKey.equals("exxab-application")) {
				return Response.ok(
						service.findCustomerByEmail(username, password))
						.build();
			} else {
				throw new IllegalArgumentException("Unauthorized call");
			}
		} catch (Exception e) {
			return handleException(e);
		}
	}
}

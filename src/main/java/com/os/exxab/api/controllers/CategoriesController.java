package com.os.exxab.api.controllers;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.code.magja.model.category.Category;
import com.google.code.magja.model.product.Product;
import com.os.exxab.api.service.CategoriesService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 */
@Path("/categories")
@Api("/categories")
@Produces(MediaType.APPLICATION_JSON)
public class CategoriesController extends GenericController{

	private CategoriesService service = new CategoriesService(); 
	
	@GET
	@ApiOperation("List All Categories")
	public Response get() {
		try {
			return Response.ok(service.listCategories()).build();
		} catch (Exception e) {
			return handleException(e);
		}
	}
	
	@GET
	@Path("/tree")
	@ApiOperation("List All Categories")
	public Response getAsTree() {
		try {
			return Response.ok(service.listCategoriesAsTree()).build();
		} catch (Exception e) {
			return handleException(e);
		}
	}

	@GET
	@Path("{id}")
	@ApiOperation("Get  Category By ID")
	public Response getCategory(@PathParam("id") Integer categoryId) {
		try {
			Category category = service.findCategory(categoryId);
			return Response.ok(category).build();
		} catch (Exception e) {
			return handleException(e);
		}
	}
	
	@GET
	@Path("{id}/products")
	@ApiOperation("Get  Category By ID")
	public Response getProductsByCategoryId(@PathParam("id") Integer categoryId) {
		try {
			List<Product> category = service.findCategoryProducts(categoryId);
			return Response.ok(category).build();
		} catch (Exception e) {
			return handleException(e);
		}
	}
}

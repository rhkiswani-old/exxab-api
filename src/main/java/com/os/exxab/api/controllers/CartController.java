package com.os.exxab.api.controllers;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.magi.magento.ws.client.CustomerAddressEntityCreate;
import com.magi.magento.ws.client.ShoppingCartCustomerAddressEntity;
import com.magi.magento.ws.client.ShoppingCartCustomerEntity;
import com.os.exxab.api.service.CartService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

/**
 */
@Path("/cart")
@Api("/cart")
@Produces(MediaType.APPLICATION_JSON)
public class CartController extends GenericController{

	private CartService service = new CartService(); 
	
	@POST
	@Path("createCart")
	@ApiOperation("Creating New Cart")
	public Response createCart() {
		try {
			return Response.ok(service.create()).build();
		} catch (Exception e) {
			return handleException(e);
		}
	}
	
	@GET
	@Path("{customerId}/address")
	@ApiOperation("Get Customer Address")
	public Response getCusomterAddress(@PathParam("customerId") Integer customerId) {
		try {
			return Response.ok(service.getAddress(customerId)).build();
		} catch (Exception e) {
			return handleException(e);
		}
	}
	
	@GET
	@Path("{cartId}/paymentMethods")
	@ApiOperation("Get Payment Methods")
	public Response getPaymentMethods(@PathParam("cartId") Integer cartId) {
		try {
			return Response.ok(service.getPaymentMethods(cartId)).build();
		} catch (Exception e) {
			
			return handleException(e);
		}
	}
	
	@GET
	@Path("{cartId}")
	@ApiOperation("Get Cart Info")
	public Response getCart(@PathParam("cartId") Integer cartId) {
		try {
			return Response.ok(service.getCart(cartId)).build();
		} catch (Exception e) {
			
			return handleException(e);
		}
	}
	
	
	@POST
	@Path("{customerId}/address")
	@ApiOperation("Add New Address")
	public Response addAddress(@PathParam("customerId") Integer customerId,CustomerAddressEntityCreate addressData ) {
		try {
			return Response.ok(service.addAddress(customerId, addressData)).build();
		} catch (Exception e) {
			
			return handleException(e);
		}
	}

	@POST
	@Path("{cartId}/{productId}/{sku}/{qty}")
	@ApiOperation("Add New Product To Cart")
	public Response addProductToCart(@PathParam("cartId") Integer cartId,@PathParam("productId") String productId,@PathParam("sku") String sku,@PathParam("qty") Double qty) {
		try {
			return Response.ok(service.addProduct(cartId, productId, sku, qty)).build();
		} catch (Exception e) {
			
			return handleException(e);
		}
	}
	
	@POST
	@Path("{cartId}/{productId}/{sku}/{qty}/remove")
	@ApiOperation("Add New Product To Cart")
	public Response removeProductFromCart(@PathParam("cartId") Integer cartId,@PathParam("productId") String productId,@PathParam("sku") String sku,@PathParam("qty") Double qty) {
		try {
			return Response.ok(service.removeProduct(cartId, productId, sku, qty)).build();
		} catch (Exception e) {
			
			return handleException(e);
		}
	}
	
	@POST
	@Path("{cartId}/checkout")
	@ApiOperation("Checkout")
	public Response checkout(@PathParam("cartId") Integer cartId) {
		try {
			return Response.ok(service.chcekout(cartId)).build();
		} catch (Exception e) {
			
			return handleException(e);
		}
	}
	
	@POST
	@Path("{cartId}/customer")
	@ApiOperation("Checkout")
	public Response checkoutCustomer(@PathParam("cartId") Integer cartId, @ApiParam(required = true)ShoppingCartCustomerEntity customer) {
		try {
			return Response.ok(service.addCustomerToCart(cartId, customer)).build();
		} catch (Exception e) {
			
			return handleException(e);
		}
	}
	
	@POST
	@Path("{cartId}/cartAddress")
	@ApiOperation("Checkout")
	public Response checkoutAddress(@PathParam("cartId") Integer cartId, @ApiParam(required = true) ShoppingCartCustomerAddressEntity customer) {
		try {
			return Response.ok(service.addAddressToCart(cartId, customer)).build();
		} catch (Exception e) {
			
			return handleException(e);
		}
	}
	
}


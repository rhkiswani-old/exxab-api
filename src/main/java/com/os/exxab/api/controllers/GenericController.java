package com.os.exxab.api.controllers;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * 
 * @author mohamed
 *
 */
public class GenericController {

	protected Response handleException(Exception e) {
		e.printStackTrace();
		return Response.status(Response.Status.UNAUTHORIZED).entity(e.getMessage()).type(MediaType.TEXT_PLAIN).build();
	}
}

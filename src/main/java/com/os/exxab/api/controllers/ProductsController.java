package com.os.exxab.api.controllers;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.os.exxab.api.pojos.ProductWithMedia;
import com.os.exxab.api.service.ProductsService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 */
@Path("/products")
@Api("/products")
@Produces(MediaType.APPLICATION_JSON)
public class ProductsController extends GenericController{

	private ProductsService service = new ProductsService(); 
	
	@GET
	@ApiOperation("List All Products")
	public Response get() {
		try {
			return Response.ok(service.listProducts()).build();
		} catch (Exception e) {
			return handleException(e);
		}
	}
	
	@GET
	@Path("{productId}")
	@ApiOperation("Get  Product By Product Id")
	public Response getProduct(@PathParam("productId") String productId) {
		try {
			ProductWithMedia product = service.findProduct(productId);
			return Response.ok(product).build();
		} catch (Exception e) {
			return handleException(e);
		}
	}
	

}

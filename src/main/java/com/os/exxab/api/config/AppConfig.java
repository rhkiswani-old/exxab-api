package com.os.exxab.api.config;

import java.io.IOException;
import java.util.Properties;

public class AppConfig extends Properties {

	private static AppConfig config = new AppConfig();

	private AppConfig() {
		try {
			load(AppConfig.class.getResourceAsStream("/magento-api.properties"));
		} catch (IOException e) {
		}
	}

	public static AppConfig getInstance() {
		return config;
	}

	public String getURL(){
		return get("magento-api-url") +"";
	}
	
	public String getUsername(){
		return get("magento-api-username") +"";
	}
	
	public String getPassword(){
		return get("magento-api-password") +"";
	}

	public String getURLV2() {
		return get("magento-api-url-v2") +"";
	}
}

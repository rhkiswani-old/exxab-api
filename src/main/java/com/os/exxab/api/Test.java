package com.os.exxab.api;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.os.exxab.api.util.Md5Helper;

public class Test {

	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
	//Mrk_9626
	public static void main(String[] args) {

		String hashedPassToStoreInDB = "bf5eaf6e457c43ce497ddbe2c62f9be7:pA1vldfCFgVJwEAydxZNWA7KyURlU80g";
	    //Password Verify
	    String inputPassword = "Mrk_9626";
	    String md5 = Md5Helper.md5("pA1vldfCFgVJwEAydxZNWA7KyURlU80g"+inputPassword);
	    System.out.println(md5);
//	    byte[] hashedInputPassword = hashPassword(inputPassword,"pA1vldfCFgVJwEAydxZNWA7KyURlU80g");
//	    System.out.println("Users hashed password: " + bytesToHex(hashedInputPassword));
//
//		if (hashedPassToStoreInDB.equals(hashedInputPassword)) {
//	        System.out.println("Correct");
//	    } else {
//	        System.out.println("Incorrect");
//	    }
	}
	
	

	private static byte[] hashPassword(String password, String salt2) {
		try {
		    MessageDigest digest = MessageDigest.getInstance("SHA-256");
		    return digest.digest((password + salt2).getBytes("UTF-8"));
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
		    return null;
		}
	}
	
	public static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    for ( int j = 0; j < bytes.length; j++ ) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}
}

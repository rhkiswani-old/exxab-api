package com.os.exxab.api;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

import com.os.exxab.api.controllers.CartController;
import com.os.exxab.api.controllers.CategoriesController;
import com.os.exxab.api.controllers.ProductsController;
import com.os.exxab.api.controllers.UsersController;

/**
 */
public class ExxabApplication extends Application<ExxabApiConfiguration> {

    public static void main(String... args) throws Exception {
        new ExxabApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<ExxabApiConfiguration> bootstrap) {
        bootstrap.addBundle(new SwaggerBundle<ExxabApiConfiguration>() {
            @Override
            protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(ExxabApiConfiguration sampleConfiguration) {
                return sampleConfiguration.swaggerBundleConfiguration;
            }
        });
    }

    @Override
    public void run(ExxabApiConfiguration configuration, Environment environment) throws Exception {
    	environment.jersey().packages("com.os.exxab.api.controllers");
//        environment.jersey().register(new ProductsController());
//        environment.jersey().register(new CategoriesController());
//        environment.jersey().register(new CartController());
//        environment.jersey().register(new UsersController());
    }
}

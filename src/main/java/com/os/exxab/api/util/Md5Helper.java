package com.os.exxab.api.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Md5Helper {

	/**
	 * 
	 * @param input
	 * @return
	 */
	public static String md5(String input) {
		String result = input;
		try {
			if (input != null) {
				MessageDigest md = MessageDigest.getInstance("MD5");
				md.update(input.getBytes());
				BigInteger hash = new BigInteger(1, md.digest());
				result = hash.toString(16);
				while (result.length() < 32) {
					result = "0" + result;
				}
			}
			return result;
		} catch (NoSuchAlgorithmException nsa) {
			return null;
		}
	}
}

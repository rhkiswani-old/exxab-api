package com.os.exxab.api.service;
import com.google.code.magja.service.ServiceException;
import com.magi.magento.ws.client.CustomerAddressEntityCreate;
import com.magi.magento.ws.client.CustomerAddressEntityItem;
import com.magi.magento.ws.client.ShoppingCartCustomerAddressEntity;
import com.magi.magento.ws.client.ShoppingCartCustomerEntity;
import com.magi.magento.ws.client.ShoppingCartInfoEntity;
import com.magi.magento.ws.client.ShoppingCartPaymentMethodResponseEntity;
import com.magi.magento.ws.client.ShoppingCartProductEntity;

/**
 * 
 * @author mohamed
 *
// */
public class CartService extends GenericService{
	

	/**
	 * 
	 * @param parseInt
	 * @return
	 * @throws ServiceException
	 */
	public int create() throws Exception {
		return api.getPort().shoppingCartCreate(api.getSessionId(), "1");
	}
	
	
	/**
	 * 
	 * @param cartId
	 * @param productId
	 * @param sku
	 * @param qty
	 * @return 
	 * @throws Exception
	 */
	public boolean addProduct(int cartId,String productId,String sku , double qty) throws Exception {
		MagentoAPI magentoAPI = api;
		ShoppingCartProductEntity[] product = new ShoppingCartProductEntity[]{new ShoppingCartProductEntity(productId,sku,qty,null,null,null,null)};
		return magentoAPI.getPort().shoppingCartProductAdd(magentoAPI.getSessionId(), cartId, product, "1");
	}
	
	/**
	 * 
	 * @param cartId
	 * @param productId
	 * @param sku
	 * @param qty
	 * @return
	 * @throws Exception
	 */
	public boolean removeProduct(int cartId,String productId,String sku , double qty) throws Exception {
		MagentoAPI magentoAPI = api;
		ShoppingCartProductEntity[] product = new ShoppingCartProductEntity[]{new ShoppingCartProductEntity(productId,sku,qty,null,null,null,null)};
		return magentoAPI.getPort().shoppingCartProductRemove(magentoAPI.getSessionId(), cartId, product, "1");
	}
	
	/**
	 * 
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public CustomerAddressEntityItem[] getAddress(int customerId) throws Exception{
		MagentoAPI magentoAPI = api;
		return magentoAPI.getPort().customerAddressList(magentoAPI.getSessionId(), customerId);
	}
	
	/**
	 * 
	 * @param cartId
	 * @return
	 * @throws Exception
	 */
	public ShoppingCartPaymentMethodResponseEntity[] getPaymentMethods(int cartId) throws Exception{
		MagentoAPI magentoAPI = api;
		return magentoAPI.getPort().shoppingCartPaymentList(magentoAPI.getSessionId(), cartId, "1");
	}
	
	/**
	 * 
	 * @param customerId
	 * @param addressData
	 * @return 
	 * @throws Exception
	 */
	public int addAddress(int customerId, CustomerAddressEntityCreate addressData) throws Exception{
		MagentoAPI magentoAPI = api;
		return magentoAPI.getPort().customerAddressCreate(magentoAPI.getSessionId(), customerId, addressData);
	}

	/**
	 * 
	 * @param cartId
	 * @return
	 * @throws Exception
	 */
	public ShoppingCartInfoEntity getCart(Integer cartId)  throws Exception{
		MagentoAPI magentoAPI = api;
		return magentoAPI.getPort().shoppingCartInfo(magentoAPI.getSessionId(), cartId, "1");
	}
	
	
	public String chcekout(Integer cartId)  throws Exception{
		MagentoAPI magentoAPI = api;
		return magentoAPI.getPort().shoppingCartOrder(magentoAPI.getSessionId(), cartId, "1", null);
	}
	
	/**
	 * 
	 * @param cartId
	 * @param customer
	 * @return
	 * @throws Exception
	 */
	public boolean addCustomerToCart(Integer cartId, ShoppingCartCustomerEntity customer)  throws Exception{
		MagentoAPI magentoAPI = api;
		return magentoAPI.getPort().shoppingCartCustomerSet(magentoAPI.getSessionId(), cartId, customer, "1");
	}
	
	/**
	 * 
	 * @param cartId
	 * @param address
	 * @return
	 * @throws Exception
	 */
	public boolean addAddressToCart(Integer cartId, ShoppingCartCustomerAddressEntity address)  throws Exception{
		MagentoAPI magentoAPI = api;
		return magentoAPI.getPort().shoppingCartCustomerAddresses(magentoAPI.getSessionId(), cartId, new ShoppingCartCustomerAddressEntity[]{}, "1");
	}

}

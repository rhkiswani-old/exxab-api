package com.os.exxab.api.service;

import java.net.MalformedURLException;

public class GenericService {

	protected static MagentoAPI api = null;
	protected static Object lock = new Object();

	public GenericService() {
		init();
	}
	
	private void init() {
		if(api == null){
			synchronized (lock) {
				if(api == null){
					try {
						api = new MagentoAPI();
					} catch (MalformedURLException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

}

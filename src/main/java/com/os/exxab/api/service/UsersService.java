package com.os.exxab.api.service;

import com.magi.magento.ws.client.AssociativeEntity;
import com.magi.magento.ws.client.CustomerCustomerEntity;
import com.magi.magento.ws.client.Filters;
import com.os.exxab.api.util.Md5Helper;

public class UsersService extends GenericService{

	public CustomerCustomerEntity[] findCustomerByEmail(String email,String password) throws Exception {
		Filters filters = new Filters();
		AssociativeEntity[] associativeEntity = new AssociativeEntity[]{(new AssociativeEntity("email", email))};
		filters.setFilter(associativeEntity);
		CustomerCustomerEntity[] customerCustomerList = api.getResources().customerCustomerList(api.getSessionId(), filters);
		if(customerCustomerList != null && customerCustomerList.length > 0){
			CustomerCustomerEntity user = customerCustomerList[0];
			String password_hash = user.getPassword_hash();
			String[] passwordParts = password_hash.split(":");
			String passwordEncodedAsMd5 = Md5Helper.md5(passwordParts[1]+password);
			if(!passwordEncodedAsMd5.equals(passwordParts[0])){
				throw new IllegalArgumentException("Invaild username or password");
			}
		}
		return customerCustomerList;
	}

}

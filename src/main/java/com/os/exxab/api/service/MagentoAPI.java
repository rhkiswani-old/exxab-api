package com.os.exxab.api.service;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import com.magi.magento.Credentials;
import com.magi.magento.service.BaseService;
import com.magi.magento.ws.client.PortType;
import com.os.exxab.api.config.AppConfig;

/**
 * 
 * @author mohamed
 *
 */
public class MagentoAPI extends BaseService {

	/**
	 * 
	 * @throws MalformedURLException
	 */
	public MagentoAPI() throws MalformedURLException {
		super(new URL(AppConfig.getInstance().getURLV2()), AppConfig.getInstance().getUsername(), AppConfig.getInstance().getPassword());
		login();
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public PortType getResources() throws Exception {
		return getPort();
	}

	/**
	 * 
	 */
	public String getSessionId() {
		logout();
		login();
		String sessionId = super.getSessionId();
		return sessionId;
	}

}

package com.os.exxab.api.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import com.google.code.magja.model.category.Category;
import com.google.code.magja.model.product.Product;
import com.google.code.magja.service.RemoteServiceFactory;
import com.google.code.magja.service.ServiceException;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.magi.magento.ws.client.CatalogProductReturnEntity;
import com.os.exxab.api.pojos.CategoryWithChildern;
import com.os.exxab.api.pojos.ProductWithMedia;
import com.os.exxab.api.pojos.SampleProduct;
import com.os.exxab.api.pojos.SimpleCategory;


/**
 * 
 * @author mohamed
 * TODO : revisit this class
 */
public class CategoriesService extends GenericService{
	private static ProductsService productsService = new ProductsService();
	public CategoriesService(){
		
	}
	
	/**
	 * 
	 */
	private static LoadingCache<String, Object> cacheA = CacheBuilder.newBuilder().maximumSize(10000).expireAfterAccess(3, TimeUnit.DAYS).recordStats()
			.build(new CacheLoader<String, Object>() {
				@Override
				public Object load(String key) throws Exception {
					if ("CATEGORIES".equals(key)) {
						return getAllCategories();
					} else if (key.contains("CATEGORIES_WITH_CHILDEREN")){
						HashMap<Integer, SimpleCategory> allCategories = (HashMap<Integer, SimpleCategory>) cacheA.get("CATEGORIES");
						CategoryWithChildern childern = new CategoryWithChildern();
						childern.setId(2);
						childern.setName("ROOT");
						fetchChilderen(childern , allCategories);
						return childern;
					} else if (key.contains("#ID")){
						return get(Integer.parseInt(key.replace("#ID", "")));
					}else if (key.contains("#PRODUCTS_BY_CAT_ID")){
						int catId =  Integer.parseInt(key.replace("#PRODUCTS_BY_CAT_ID", ""));
						Category category = get(catId);
						List<Product> products = RemoteServiceFactory.getSingleton().getCategoryRemoteService().getProducts(category);
						List<SampleProduct> productsWithFullDetails = new ArrayList<SampleProduct>();
						for (Product product : products) {
							if(product == null){
								continue;
							}
							SampleProduct findProduct = productsService.populateSampleProduct(product);
							productsWithFullDetails.add(findProduct);
						}
						return productsWithFullDetails;
					}
					return null;

				}
			});

	/**
	 * 
	 * @return
	 */
	private static Object getAllCategories() throws Exception{
		HashMap<Integer, SimpleCategory> hashMap = new HashMap<Integer, SimpleCategory>();
		Category rootCategory = RemoteServiceFactory.getSingleton().getCategoryRemoteService().getByIdWithChildren(2);
		listAllCategoriesWithChildern(rootCategory,hashMap);
		return hashMap;
	}

	/**
	 * 
	 * @param rootCategory
	 * @param hashMap
	 * @throws ServiceException
	 */
	private static void listAllCategoriesWithChildern(Category rootCategory, HashMap<Integer, SimpleCategory> hashMap) throws ServiceException {
		List<Category> children = rootCategory.getChildren();
		for (Category category : children) {
			Category catWithChildern = RemoteServiceFactory.getSingleton().getCategoryRemoteService().getByIdWithChildren(category.getId());
			if(catWithChildern.getChildren().size() > 0){
				listAllCategoriesWithChildern(catWithChildern,hashMap);
			}
			SimpleCategory simpleCategory = new SimpleCategory();
			simpleCategory.setId(category.getId());
			simpleCategory.setParentId(category.getParent().getId());
			simpleCategory.setName(category.getName());
			System.out.println("=======================================================");
			System.out.println(simpleCategory);
			System.out.println("=======================================================");
			hashMap.put(category.getId(), simpleCategory);
		}
		
	}

	/**
	 * 
	 * @return
	 */
	private static Category get(int categoryId) throws Exception{
		return RemoteServiceFactory.getSingleton().getCategoryRemoteService().getByIdClean(categoryId);
	}

	/**
	 * 
	 * @return
	 * @throws ServiceException
	 */
	public HashMap<Integer, SimpleCategory> listCategories() throws Exception{
		return (HashMap<Integer, SimpleCategory>) cacheA.get("CATEGORIES");
	}
	
	/**
	 * 
	 * @return
	 * @throws ServiceException
	 */
	public CategoryWithChildern listCategoriesAsTree() throws Exception{
		return (CategoryWithChildern) cacheA.get("CATEGORIES_WITH_CHILDEREN");
		
	}

	/**
	 * 
	 * @param childern
	 * @param map
	 */
	private static void fetchChilderen(CategoryWithChildern childern, HashMap<Integer, SimpleCategory> map) {
		for (Integer categoryId: map.keySet()) {
			SimpleCategory simpleCategory = map.get(categoryId);
			if(childern.getId() == simpleCategory.getParentId()){
				CategoryWithChildern child = new CategoryWithChildern();
				child.setId(categoryId);
				child.setName(simpleCategory.getName());
				childern.addChild(child);
				fetchChilderen(child, map);
			}
		}
	}

	/**
	 * 
	 * @param categoryId
	 * @return
	 */
	public Category findCategory(int categoryId) {
		try {
			return (Category)cacheA.get(categoryId + "#ID");
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 
	 * @param categoryId
	 * @return
	 * @throws Exception
	 */
	public List<Product> findCategoryProducts(int categoryId) throws Exception {
		try {
			return (List<Product>)cacheA.get(categoryId + "#PRODUCTS_BY_CAT_ID");
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		CategoriesService service = new CategoriesService();
		HashMap<Integer, SimpleCategory> listCategories = service.listCategories();
		for (Integer string : listCategories.keySet()) {
			System.out.println(listCategories.get(string));
		}
		
	}

	

}

package com.os.exxab.api.service;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.google.code.magja.model.product.Product;
import com.google.code.magja.model.product.ProductMedia;
import com.google.code.magja.service.RemoteServiceFactory;
import com.google.code.magja.service.ServiceException;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.magi.magento.ws.client.AssociativeEntity;
import com.magi.magento.ws.client.CatalogInventoryStockItemEntity;
import com.magi.magento.ws.client.CatalogProductRequestAttributes;
import com.magi.magento.ws.client.CatalogProductReturnEntity;
import com.os.exxab.api.pojos.ProductWithMedia;
import com.os.exxab.api.pojos.SampleProduct;

/**
 * 
 * @author mohamed
 *
// */
public class ProductsService extends GenericService{
	

	private static final String NA = "N/A";
	private static LoadingCache<String, Object> cacheA = CacheBuilder.newBuilder().maximumSize(10000).expireAfterAccess(3, TimeUnit.DAYS).recordStats()
			.build(new CacheLoader<String, Object>() {
				ProductsService service = new ProductsService();
				@Override
				public Object load(String key) throws Exception {
					if ("PRODUCTS".equals(key)) {
						return service.getAllProducts();
					} else if (key.contains("#inventory")){
						return service.getInventoryInfo(key.split("#")[0]);
					}else{
						return service.get(key);
					}
				}

			});

	/**
	 * 
	 * @param parseInt
	 * @return
	 * @throws ServiceException
	 */
	public ProductWithMedia get(String sku) throws Exception {
		Product product = RemoteServiceFactory.getSingleton().getProductRemoteService().getBySku(sku);
		List<ProductMedia> media =  RemoteServiceFactory.getSingleton().getProductMediaRemoteService().listByProduct(product);
		List<String> urls = new ArrayList<String>();
		for (ProductMedia productMedia : media) {
			urls.add(productMedia.getUrl());
		}
		ProductWithMedia productWithMedia = new ProductWithMedia();
		productWithMedia.setId(product.getId());
		productWithMedia.setImages(urls);
		productWithMedia.setName(product.getName() != null? product.getName() : NA);
		productWithMedia.setSku(product.getSku() != null? product.getSku() : NA);
		CatalogProductRequestAttributes attributes = new CatalogProductRequestAttributes(null, new String[]{"color","condition","cost","country_of_manufacture","delivery_time","description","free_shipping" , "size"});
		CatalogProductReturnEntity findProduct=  api.getResources().catalogProductInfo(api.getSessionId(), product.getId()+"",null, attributes, null);
		CatalogInventoryStockItemEntity productInventoryInfo = findProductInventoryInfo(product.getId()+"");
		productWithMedia.setQty(productInventoryInfo.getQty() != null? Double.parseDouble(productInventoryInfo.getQty()) : 0d);
		productWithMedia.setStockAvailability(productWithMedia.getQty() > 0 ? "available" : "not available");
		productWithMedia.setPrice(findProduct.getPrice() != null? Double.parseDouble(findProduct.getPrice()) : 0d);
		productWithMedia.setShortDesc(findProduct.getShort_description() != null? findProduct.getShort_description() : NA);
		productWithMedia.setDescrition(findAttribute(findProduct.getAdditional_attributes(),"description"));
		productWithMedia.setStatus(findProduct.getStatus() != null? findProduct.getStatus() : NA);
		productWithMedia.setSpecialPrice(findProduct.getSpecial_price() != null? findProduct.getSpecial_price() : NA);
		productWithMedia.setWeight(findProduct.getWeight() != null? Double.parseDouble(findProduct.getWeight()):0d);
		productWithMedia.setColor(findAttribute(findProduct.getAdditional_attributes(),"color"));
		productWithMedia.setCondition(findAttribute(findProduct.getAdditional_attributes(),"condition"));
		productWithMedia.setCost(findAttribute(findProduct.getAdditional_attributes(),"cost"));
		productWithMedia.setSize(findAttribute(findProduct.getAdditional_attributes(),"size"));
		return productWithMedia;
	}

	/**
	 * 
	 * @param productId
	 * @return
	 * @throws Exception
	 */
	protected CatalogInventoryStockItemEntity getInventoryInfo(String productId) throws Exception {
		return api.getResources().catalogInventoryStockItemList(api.getSessionId(), new String[]{productId})[0];
	}

	/**
	 * 
	 * @return
	 * @throws ServiceException
	 */
	public List<SampleProduct> getAllProducts() throws Exception {
		List<Product> list = RemoteServiceFactory.getSingleton().getProductRemoteService().listAllNoDep();
		List<SampleProduct> sampleProducts = new  ArrayList<SampleProduct>();
		for (Product product : list) {
			SampleProduct sampleProduct = populateSampleProduct(product);
			sampleProducts.add(sampleProduct);
		}
		return sampleProducts;
	}

	/**
	 * 
	 * @param product
	 * @return
	 */
	public SampleProduct populateSampleProduct(Product product) {
		SampleProduct sampleProduct = new SampleProduct();
		sampleProduct.setId(product.getId());
		sampleProduct.setName(product.getName());
		sampleProduct.setSku(product.getSku());
		return sampleProduct;
	}

	private String findAttribute(AssociativeEntity[] additional_attributes, String attribute) {
		for (AssociativeEntity associativeEntity : additional_attributes) {
			if(associativeEntity.getKey().equalsIgnoreCase(attribute)){
				return associativeEntity.getValue();
			}
		}
		return NA;
	}

	/**
	 * 
	 * @return
	 * @throws ServiceException
	 */
	public List<Product> listProducts() throws Exception {
		return (List<Product>) cacheA.get("PRODUCTS");
	}

	/**
	 * 
	 * @param productId
	 * @return
	 * @throws ServiceException
	 */
	public ProductWithMedia findProduct(String productId) throws Exception {
		return (ProductWithMedia) cacheA.get(productId);
	}
	
	/**
	 * 
	 * @param productId
	 * @return
	 * @throws ServiceException
	 */
	public CatalogInventoryStockItemEntity findProductInventoryInfo(String productId) throws Exception {
		return (CatalogInventoryStockItemEntity) cacheA.get(productId+"#inventory");
	}
}
